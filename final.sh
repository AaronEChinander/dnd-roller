#!/bin/bash

DieNumber=$1 #user provides how many die to roll
DieType=$2 #user provides the kind of die to roll (d20, d6, d8 etc)
DieNumber2=$3 #user provides second amount of die to roll
DieType2=$4 #user provides the kind of die to roll

if [ "$#" -eq 0 ]; then #prints help for using the script
	echo "This script rolls a number of standard die using up to 4 command line arguments. The 1st and 3rd command line argument specifies how many die the user wants to roll and the 2nd and 4th command line argument specifies the type of die. For example, entering 20 will roll a 20 sided die."

else

for x in $(seq 1 $DieNumber); do
	echo "Rolling dice..."
	DieResult=$(($RANDOM % $2 +1)) #calculates the dice roll
		if [ $2 -a $DieResult -eq 20 ]; then
			echo -e "\e[1;5;32m"$DieResult" CRITICAL HIT! \e[0m"
		elif [ $DieResult -eq 1 ]; then
			echo -e "\e[2;31m"$DieResult" Critical Fail... \e[0m"
		else
			echo "$DieResult"
		fi
done
for x in $(seq 1 $DieNumber2); do
	echo -e "\e[7mRolling dice... \e[0m" #inverts the colors to differentiate pools
	DieResult2=$(($RANDOM % $4 +1)) #calculates the dice roll
		if [ $4 -a $DieResult2 -eq 20 ]; then
			echo -e "\e[1;5;32m"$DieResult2" CRITICAL HIT! \e[0m"
		elif [ $DieResult2 -eq 1 ]; then
			echo -e "\e[2;31m"$DieResult2" Critical Fail... \e[0m"
		else
			echo -e "\e[7m$DieResult2 \e[0m"
		fi
done
fi
